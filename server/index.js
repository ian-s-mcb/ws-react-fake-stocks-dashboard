const express = require('express')
const { Server } = require('ws')
const path = require('path')

const PORT = process.env.PORT || 5000
const app = express()

// Priority serve any static files.
app.use(express.static(path.resolve(__dirname, '../react-ui/build')));

// Answer API requests.
app.get('/api', function (req, res) {
  res.set('Content-Type', 'application/json');
  res.send('{"message":"Hello from the custom server!"}');
});

// All remaining requests return the React app, so it can handle routing.
app.get('*', function(request, response) {
  response.sendFile(path.resolve(__dirname, '../react-ui/build', 'index.html'));
});

const server = app.listen(PORT, function () {
  console.error(`Listening on port ${PORT}`);
});

const wss = new Server({ server })

// Globals
const stocks = {
  "AAPL": 95.0,
  "MSFT": 50.0,
  "AMZN": 300.0,
  "GOOG": 550.0,
  "YHOO": 35.0,
}
let stockUpdater

// Helper random number generator function
function randomInterval(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min)
}

// Update stocks global according to randomized changes that occur at a
// randomized frequency
function randomStockUpdater() {
  for (let symbol in stocks) {
    if (stocks.hasOwnProperty(symbol)) {
      let randomizedChange = randomInterval(-150, 150)
      let floatChange = randomizedChange / 100
      stocks[symbol] += floatChange
      // stocks[symbol].toFixed(2)
    }
  }
  let randomMSTime = randomInterval(500, 2500)
  stockUpdater = setTimeout(
    () => randomStockUpdater(),
    randomMSTime
  )
}

// Handle ws connection
wss.on('connection', ws => {
  console.log('Client connected')
  let clientStockUpdater
  let clientStocks = []
  // Helper for sending updated stocks to client
  function sendStockUpdates (ws) {
    if (ws.readyState == 1) {
      let stockObj = {}

      for (let i = 0; i < clientStocks.length; i++) {
        let symbol = clientStocks[i]
        stockObj[symbol] = stocks[symbol]
      }
      ws.send(JSON.stringify(stockObj))
    }
  }
  // Repeatedly sends updated stocks at frequency of 1 sec
  clientStockUpdater = setInterval(() => sendStockUpdates(ws), 1000)

  // Respond the client's message
  // Message specifies the stocks symbols the client wants the server to
  // include in the server updates
  ws.on('message', message => {
    console.log('Received message: ', message)
    let stockRequest = JSON.parse(message)
    clientStocks = stockRequest['stocks']
    sendStockUpdates(ws)
  })
  // Respond to closing of client connection
  // Termintates the stock update interval
  ws.on('close', () => {
    console.log('onclose')
    if (typeof clientStockUpdater !== 'undefined') {
      clearInterval(clientStockUpdater)
    }
  })
})

// Start the stock update interval
randomStockUpdater()
