import React, { useEffect, useState } from 'react';

import ws from './utils'
import './App.css'

// Gloabl constants
const stockRequest = {
  stocks: [
    'AAPL',
    'MSFT',
    'AMZN',
    'GOOG',
    'YHOO',
  ],
}
const initialStocks = {
  'AAPL': 0,
  'MSFT': 0,
  'AMZN': 0,
  'GOOG': 0,
  'YHOO': 0,
}

function App (props) {
  const [prevStocks, setPrevStocks] = useState({...initialStocks})
  const [stocks, setStocks] = useState({...initialStocks})

  // Setup WebSocket event listeners via one-time executing effect
  useEffect(() => {
    ws.onopen = e => {
      console.log('onopen')
      ws.send(JSON.stringify(stockRequest))
    }
    ws.onclose = e => {
      console.log('onclose')
      setPrevStocks(initialStocks)
      setStocks(initialStocks)
    }
    ws.onmessage = e => {
      const newStocks = JSON.parse(e.data)
      setPrevStocks(stocks)
      setStocks(newStocks)
    }
    ws.onerror = e => {
      console.log('onerror')
      throw new Error('WebSocket error', e)
    }
  })

  const stockList = Object.entries(stocks).map(([ symbol, price ], i) => (
    <tr key={i} className={price > prevStocks[symbol] ? 'table-success' : 'table-danger'}>
      <td id={symbol}>{symbol}</td>
      <td>{price.toFixed(2)}</td>
    </tr>
  ))

  return (
    <div className="App">
      <h1>Stocks Dashboard</h1>
      <table className="table">
        <thead>
          <tr>
            <th>Symbol</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {stockList}
        </tbody>
      </table>
    </div>
  );
}

export default App
