# ws-react-fake-stocks-dashboard

A dashboard of fake, time-series stock data broadcast from a WebSocket
server to a React (hook-based) frontend.

### [Demo][demo]

### Screenshot
![screenshot][screenshot]

### Technologies used

- [React frontend][react]
- [create-react-app][create-react-app] - scaffolding for single page apps
- Node and [Express][express] backend
- [Heroku][heroku] - host for the backend
- [ws][ws] - an implmentation of a WebSocket server
- [react-websocket][react-websocket] - a web component that wraps the
  browser's built-in WebSocket client

[demo]: https://serene-lake-63955.herokuapp.com/
[screenshot]: https://i.imgur.com/l0QRile.gif
[heroku-node-and-react]: https://github.com/mars/heroku-cra-node
[heroku-node-and-ws]: https://github.com/heroku-examples/node-ws-test
[react]: https://reactjs.org/
[create-react-app]: https://github.com/facebook/create-react-app
[express]: https://github.com/expressjs/express
[heroku]: https://www.heroku.com/
[ws]: https://github.com/websockets/ws
[react-websocket]: https://github.com/mehmetkose/react-websocket
